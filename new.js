import { Selector } from 'testcafe';

fixture `Getting Started`
    .page `https://www.google.co.uk`;

test('My first test', async t => {
    // prm-pt
    await t.expect(Selector('#prm-pt').exists).ok();
});
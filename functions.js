import { Selector } from 'testcafe';
import { baseSiteUrl } from './global-variables.js';

export async function dismissCookieConsent(t) {
    // Click to accept cookies
    await t.click(Selector('#st-cookie-accept'));

    // Wait until the popup is no longer visible 
    await t.expect(Selector('#cookieConsent').exists).notOk();
}
export async function logInTestUser(t, adminUser, correctPassword) {
    var email;
    var password;
    if (adminUser) {
        email = 'test.admin@YOURSITE.com';
        if (correctPassword) {
            password = 'password12345admin'; // Change test admin user password here
        }
        else {
            password = 'password123';
        }
    }
    else {
        email = 'test.customer@YOURSITE.com';
        if (correctPassword) {
            password = 'password12345'; // Change test admin user password here
        }
        else {
            password = 'password123';
        }
    }

    // Click the sign in button
    await t
        .hover(Selector('#js-user-settings'))
        .click(Selector('#st-sign-in'));

    // Select the email input and input the test customer email
    //await t.typeText('[name=Email]', email);
    await t.typeText('[name=Email]', email, { paste: true }) 

    // Select the password input and input the test customer password
    await t.typeText('[name=Password]', password);

    // Click the login button
    await t.click(Selector('[name=Login]'));

    // Wait for page to be reloaded and the user to be logged in
    await Selector('#st-my-account').with({ visibilityCheck: true })();
}
export async function addItemToBasketFromSkuPage(t, productCode, productQty) {
    // Go to a SKU page
    await t.navigateTo(baseSiteUrl + productCode);

    // Select the product quantity input and input the product quantity
    await t.typeText('#js-product-qty', productQty);

    // Click the add to basket button and dismiss the configurator prompt if necessary
    await t
        .setNativeDialogHandler(() => false) //using false here will dismiss the confirmation dialogue rather than accepting it
        .click(Selector('#addToBasketButton'));
}
export async function changeItemQuantityFromBasketPage(t, productCode, newQty) {
    // Go to the basket page
    await t.navigateTo(baseSiteUrl + 'Checkout/Basket');

    // Wait until the page has reloaded and the quantity input is available
    // Select the quantity input and input the new quantity
    await t.typeText('#st-qty-' + productCode, newQty, { replace: true });

    // Click the add button
    await t.click(Selector('#st-update-qty-' + productCode));

    // Wait for page to be reloaded
    await Selector('#st-updateQty-' + productCode).with({ visibilityCheck: true })();
}
export async function emptyBasket(t) {
    // Go to the basket page
    await t.navigateTo(baseSiteUrl + 'Checkout/Basket');

    // Handle the alert that appears asking the user to confirm they want to empty the basket
    // Click the empty  button
    await t
        .setNativeDialogHandler(() => true)
        .click(Selector('#clearBasket'));

    // Check that the empty basket text is present (assertion with timeout)
    await t.expect(Selector('#st-empty-basket-message').withText('YOUR BASKET IS EMPTY').exists).ok({ timeout: 5000 });
}
export async function changeInvoiceAddressCountryFromBasket(t, countryName, countryCode, alreadyOnBasket) {
    if (!alreadyOnBasket) {
        // Go to the basket page and click the checkout button
        await t
            .navigateTo(baseSiteUrl + 'Checkout/Basket')
            .click(Selector('.basket-confirm-button'));
    }

    await t.click(Selector('#invoiceAddressEditButton'));

    const countrySelect = Selector('[name=CountryCode]');
    const countryOption = countrySelect.find('option');

    await t
        .click(countrySelect)
        .click(countryOption.withText(countryName))
        .expect(countrySelect.value).eql(countryCode);

    await t.click(Selector('#addressSaveButton'));
}
export async function checkPandPOptionNotPresentInBasket(t, pandpName) {
    // Go to the basket page and click the checkout button
    await t
        .navigateTo(baseSiteUrl + 'Checkout/Basket')
        .click(Selector('.basket-confirm-button'));
        
    const pandpSelect = Selector('[name=pandPOptions]');
    const pandpOption = pandpSelect.find('option');
    const pandpOptionCount = await pandpSelect.find('option').count;

    for (var i = 0; i < pandpOptionCount; i++) {
        await t.expect(pandpOption.nth(i).textContent).notContains(pandpName, pandpName + ' delivery option found');
    }
}
async function asyncDateString() {
    // Get current date and time 
    var d = new Date();

    // Remove the non-email-friendly characters
    return new Promise((resolve, reject) => {
        resolve(d.getFullYear().toString() + d.getMonth().toString() + d.getDate().toString() + d.getHours().toString() + d.getMinutes().toString() + d.getMilliseconds().toString())
    });
}
export async function getDateString() {
    const result = await asyncDateString();
    return result;
}
async function asyncShorterDateString() {
    // Get current date and time 
    var d = new Date();

    // Remove the non-email-friendly characters
    return new Promise((resolve, reject) => {
        resolve(d.getMonth().toString() + d.getDate().toString() + d.getHours().toString() + d.getMinutes().toString())
    });
}
export async function getShorterDateString() {
    const result = await asyncShorterDateString();
    return result;
}
export async function searchForTerm(t, searchTerm) {
    // Select the search input and input the search term
    await t
        .typeText('#js-search-wrap', searchTerm)
        .pressKey('enter');
}
export async function placeOrder(t, productCode, productQty) {
    // Place an order 
    await module.exports.addItemToBasketFromSkuPage(t, productCode, productQty);

    // Go to the basket page and click the checkout button
    await t
        .navigateTo(baseSiteUrl + 'Checkout/Basket')
        .click(Selector('.basket-confirm-button'));

    // Select the reference number input and input 'Test'
    await t.typeText('[name=CustomerOrderNumber]', 'Test');

    // Click #PaymentButton and handle the popup (dismiss it rather than accept it)
    await t
        .click(Selector('#st-pay-account')) //This will only work using an admin account
        .click(Selector('#st-privacy-agree'))
        .expect(Selector('#paymentButtonCheck').checked).ok()
        .setNativeDialogHandler(() => true) //using false here will dismiss the confirmation dialogue rather than accepting it
        .click(Selector('#PaymentButton'));
}
export async function setOrderStatusToTest(t, webOrderNumber) {
    // Log out the user 
    await logOutTestUser(t);

    // Log in the admin user 
    await logInTestUser(t, true, true);

    // Go to the orders page
    await t.navigateTo(baseSiteUrl + 'Orders');

    // Find a link with text matching the order number and click it
    await t.click(Selector('td').withText(webOrderNumber));

    // Click the edit icon(currently has class fa fa-pencil-square-o but will need an id adding for this)
    await t.click(Selector('#EditOrderStatus'));

    // Change the status id
    const pandpSelect = Selector('[name=StatusId]');
    const pandpOption = pandpSelect.find('option');

    await t
        .click(pandpSelect)
        .click(pandpOption.withText('Test'))
        .expect(pandpSelect.value).eql('4864');

    // Click the update button, reloading the page
    await t.click(Selector('#UpdateStatusId'));

    // Wait for the page to reload
    await t.expect(Selector('td').withText(webOrderNumber)).ok();

    // Find a link with text matching the order number and click it
    await t.click(Selector('td').withText(webOrderNumber));

    // Expect the id processed_Status to equal 'Status: Test', otherwise flag it as failing.
    await t.expect(Selector('#processed_Status').child('b').withText('Test').exists).ok({ timeout: 5000 });
}
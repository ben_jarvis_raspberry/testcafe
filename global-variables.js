// Set the environment here: 'dev', 'uat', 'alpha', 'beta', 'live'
var env = 'dev';

switch (env) {
    case 'dev':
        exports.baseSiteUrl = 'http://localhost:63747/'; 
        break;
    case 'beta':
        exports.baseSiteUrl = 'https://beta.YOURSITE.com/';
        break;
    case 'live':
        exports.baseSiteUrl = 'https://www.YOURSITE.com/';
        break;
    default:
        exports.baseSiteUrl = 'https://www.YOURSITE.com/';
}

// Put all global variables here

if (env == 'dev' || env == 'uat') {
    // Dev variables

    // Product types 
    exports.validProductCode = '01-0001';
    exports.otherValidProductCode = '01-0002';
    exports.nonExistantProductCode = '01-0003';


}
else {
    // Live variables

    // Product types 
    exports.validProductCode = '00-0001';
    exports.otherValidProductCode = '00-0002';
    exports.nonExistantProductCode = '00-0003';
}
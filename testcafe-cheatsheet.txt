=== Check you are on a specific url === 
import { ClientFunction } from 'testcafe';
-----
const getLocation = ClientFunction(() => document.location.href);
await t.expect(getLocation()).contains('http://theurlyou/want');

=== Call another local function within functions.js ===
await module.exports.nameOfFunction();


import { Selector } from 'testcafe';

// Import functions
import {
    dismissCookieConsent, logInTestUser, logOutTestUser, getDateString
} from '../functions.js';

// Import global variables
import {
    baseSiteUrl
} from '../global-variables.js';

fixture `Sign In / Register tests`
    .page(baseSiteUrl)
    .beforeEach(async t => {
        await t.maximizeWindow();
    });

test('SigninRegister-001 - Check that postcode field is not required when country is IE.', async t => {
    //await t.setTestSpeed(0.5);
    await dismissCookieConsent(t);

    // Go to the registration page 
    await t.navigateTo(baseSiteUrl + 'SignIn/Register');

    // Select the email input and input a test email address
    var dateString = await getDateString();
    await t.typeText('.st-register-email', 'test-'+ dateString + '@YOURWEBSITEHERE');

    // Enter password and confirm password
    await t
        .typeText('.st-register-password', 'password12345')
        .typeText('.st-register-password-confirm', 'password12345');

    // Fill Form Details
    await t.typeText('[name=Title]', 'Mr');
    await t.typeText('[name=Forename]', 'Test');
    await t.typeText('[name=Surname]', 'Test');

    // Country - Ireland
    await t
        .click(Selector('[name=CountryCode]'))
        .click(Selector('[name=CountryCode]').find('option').withText('Ireland'));

    // Check Postcode field exists
    await t.expect(Selector('[name=PostCode]').visible).ok();

    await t.typeText('[name=Address1]', 'Test');
    await t.typeText('[name=City]', 'Test');
    await t.typeText('[name=Telephone]', '01206751166');

    // Tick agree box
    await t
    // Need to click the span as the actual checkbox is not visible (for formatting reasons) meaning it cannot be clicked by testcafe
        .click(Selector('#signupSubmitCheck').parent())
        .expect(Selector('#signupSubmitCheck').checked).ok();

    // Click Register
    await t.click(Selector('#signupSubmit'));

    // Check that the My account link is present (assertion with timeout)
    await t.expect(Selector('.dash-myaccount-link').exists).ok({ timeout: 5000 });
});

test('SigninRegister-002 - Check that when a postcode is entered for an Irish address it is saved.', async t => {
    //await t.setTestSpeed(0.5);
    await dismissCookieConsent(t);

    // Go to the registration page 
    await t.navigateTo(baseSiteUrl + 'SignIn/Register');

    // Select the email input and input a test email address
    var dateString = await getDateString();
    await t.typeText('.st-register-email', 'test-'+ dateString + '@YOURWEBSITEHERE');

    // Enter password and confirm password
    await t
        .typeText('.st-register-password', 'password12345')
        .typeText('.st-register-password-confirm', 'password12345');

    // Fill Form Details
    await t.typeText('[name=Title]', 'Mr');
    await t.typeText('[name=Forename]', 'Test');
    await t.typeText('[name=Surname]', 'Test');

    // Country - Ireland
    await t
        .click(Selector('[name=CountryCode]'))
        .click(Selector('[name=CountryCode]').find('option').withText('Ireland'));

    // Check Postcode field exists
    await t.expect(Selector('[name=PostCode]').visible).ok();

    // Enter Postcode
    var postCodeString = 'IE1 1IE';
    await t.typeText('[name=PostCode]', postCodeString);
    await t.typeText('[name=Address1]', 'Test');
    await t.typeText('[name=City]', 'Test');
    await t.typeText('[name=Telephone]', '01206751166');

    // Tick agree box
    await t
    // Need to click the span as the actual checkbox is not visible (for formatting reasons) meaning it cannot be clicked by testcafe
        .click(Selector('#signupSubmitCheck').parent())
        .expect(Selector('#signupSubmitCheck').checked).ok();

    // Click Register
    await t.click(Selector('#signupSubmit'));

    // Check that the My account link is present (assertion with timeout)
    await t.expect(Selector('.dash-myaccount-link').exists).ok({ timeout: 5000 });

    // Sign out
    await logOutTestUser(t);

    // Log in as admin
    await logInTestUser(t, true, true);

    // Go to Staff Admin - Customers
    await t.navigateTo(baseSiteUrl + 'Administration/Customers');

    // Check customer that registerd if postcode is present
    // Find specific text in a td (look for the recipient email generated)
    await t.expect(Selector('td').withText('test-'+ dateString + '@YOURWEBSITEHERE').parent().child(5).withExactText(postCodeString).exists).ok({ timeout: 5000 });
});




// .after( async t => {
//     // Empty the user's basket to ensure a clean slate for other tests
//     await emptyBasket(t);
// });
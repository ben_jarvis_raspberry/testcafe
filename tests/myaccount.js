import { Selector } from 'testcafe';

// import functions 
import {
    dismissCookieConsent, logInTestUser, logOutTestUser, addItemToBasketFromSkuPageWithoutQuantity, emptyBasket, placeOrder, getDateString, setOrderStatusToTest
} from '../functions.js';

// Import global variables 
import {
    baseSiteUrl, validProductCode
} from '../global-variables.js';

fixture.skip`My Account tests`
    .page(baseSiteUrl)
    .beforeEach(async t => {
        await t.maximizeWindow();
    });

test('MyAccount-001 - Check a previously ordered item can be added to the basket.', async t => {
    //await t.setTestSpeed(0.5);
    await dismissCookieConsent(t);
    await logInTestUser(t, true, true);

    await placeOrder(t, validProductCode);

    const webOrderNumber = await Selector('#WebOrderNumber').textContent;
    // Set the order status to test
    await setOrderStatusToTest(t, webOrderNumber);

    // Navigate to the my orders page 
    await t.navigateTo(baseSiteUrl + 'MyAccount/Orders');

    // Click on an order 
    await t.click(Selector('.expandRow'));

    // Click 'add to basket' 
    await t.click(Selector('a').withText('Add To Basket'));

    // Navigate to the basket page 
    await t.navigateTo(baseSiteUrl + 'Checkout/Basket');

    // Check that the remove item button is present (assertion with timeout)
    await t.expect(Selector('#removeItem-' + validProductCode).exists).ok({ timeout: 5000 });
})
    .after(async t => {
        // Empty the user's basket to ensure a clean slate for other tests
        await emptyBasket(t);
    });

test('MyAccount-002 - Check a saved basket (job quote) can be deleted.', async t => {
    //await t.setTestSpeed(0.5);
    await dismissCookieConsent(t);
    await logInTestUser(t, false, true);

    // Add item to basket
    await addItemToBasketFromSkuPageWithoutQuantity(t, validProductCode);

    // Save basket without clearing it
    // Open the save basket popup
    await t.click(Selector('#saveBasketLink'));

    const dateString = await getDateString();

    // Select the basket name input and input a name
    await t.typeText('[name=BasketName]', 'Test Basket ' + dateString);
    //// Do not clear the basket
    // Click the save button
    await t.click(Selector('#saveBasketButton'));

    // Check saved basket with correct items exists in MyAccount
    await t.navigateTo('../../MyAccount/SavedBaskets');
    // Find specific text in a td (look for the saved basket generated)
    const savedBasket = Selector('td').withText('Test Basket ' + dateString);
    await t.expect(savedBasket.exists).ok({ timeout: 5000 });
    await t.click(savedBasket);

    // Delete basket 
    await t
        .setNativeDialogHandler(() => true) //using false here will dismiss the confirmation dialogue rather than accepting it
        .click(savedBasket.parent().child(3).child('a'));

    // Check that the basket is not present (assertion with timeout)
    await t.expect(savedBasket.exists).notOk({ timeout: 5000 });
})
    .after(async t => {
        // Empty the user's basket to ensure a clean slate for other tests
        await emptyBasket(t);
    });

test('MyAccount-003 - Check a saved basket (job quote) can be searched for by name.', async t => {
    //await t.setTestSpeed(0.5);
    await dismissCookieConsent(t);
    await logInTestUser(t, false, true);

    // Add item to basket
    await addItemToBasketFromSkuPageWithoutQuantity(t, validProductCode);

    // Save basket without clearing it
    // Open the save basket popup
    await t.click(Selector('#saveBasketLink'));

    const dateString = await getDateString();

    // Select the basket name input and input a name
    await t.typeText('[name=BasketName]', 'Test Basket ' + dateString);
    //// Do not clear the basket
    // Click the save button
    await t.click(Selector('#saveBasketButton'));

    // Navigate to the saved baskets page
    await t.navigateTo('../../MyAccount/SavedBaskets');
    // Find specific text in a td (look for the saved basket generated)
    const savedBasket = Selector('td').withText('Test Basket ' + dateString);
    await t.expect(savedBasket.exists).ok({ timeout: 5000 });
    await t.click(savedBasket);

    // Delete basket 
    await t
        .setNativeDialogHandler(() => true) //using false here will dismiss the confirmation dialogue rather than accepting it
        .click(savedBasket.parent().child(3).child('a'));

    // Check that the basket is not present (assertion with timeout)
    await t.expect(savedBasket.exists).notOk({ timeout: 5000 });
})
    .after(async t => {
        // Empty the user's basket to ensure a clean slate for other tests
        await emptyBasket(t);
    });


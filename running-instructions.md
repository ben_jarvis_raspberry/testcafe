# Testcafe Running Instructions

Firstly, ensure you have run `npm install` from the testcafe folder.

Run the below code after cd-ing to the testcafe folder, to run the tests on chrome, and output the results to a text file. Change `chrome` to `all` to do all browsers, or to `firefox` or `ie` for a specific browser.

`node --max-http-header-size=9999999 node_modules/testcafe/bin/testcafe chrome tests/ > test-results.txt -e`

This will do all tests in the tests folder (each suite is in a different files.

`.only` and `.skip` can be used within those files on entire fixtures(suites) or individual tests

https://devexpress.github.io/testcafe/documentation/using-testcafe/common-concepts/concurrent-test-execution.html
